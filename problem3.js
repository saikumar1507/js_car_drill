const inventory = require('./inventory.js')

let car_models = []
function car_model(inventory) {
    for (let cars of inventory) {
        car_models.push(cars['car_model'])
    }

}
car_model(inventory)

let car_model_lowerCase = []

for (let car of car_models) {
    car_model_lowerCase.push(car.toLowerCase())
}

const sorted_cars_model = car_model_lowerCase.sort()


module.exports = sorted_cars_model