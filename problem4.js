const inventory = require('./inventory')

function carYear(inventory){
    let all_cars_years=[]
    for(let year of inventory){
        all_cars_years.push(year['car_year'])
    }
    return all_cars_years
}
module.exports = carYear

