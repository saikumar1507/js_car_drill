const inventory = require('./inventory.js')


function Audi_BMW(inventory) {
    let only_Audi_BMW_cars = []
    for (let car of inventory) {
        if (car['car_make'] == 'Audi' || car['car_make'] == 'BMW') {
            only_Audi_BMW_cars.push(car['car_make'])
        }
    }
    return only_Audi_BMW_cars
}
module.exports = Audi_BMW