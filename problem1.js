const inventory = require('./inventory.js')

function Particular_car_info(inventory,car_id) {
    for (let id=0;id<=inventory.length;id++) {
        if (inventory[id].id == car_id) {
            return( `Car ${inventory[id].id} is a ${inventory[id].car_year} ${inventory[id].car_make}  ${inventory[id].car_model}`)
        }
    }
}

module.exports = Particular_car_info