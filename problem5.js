const inventory = require('./inventory.js')
const All_car_years = require('./problem4.js')


let All_cars_year = All_car_years(inventory)


function old_cars(All_cars_year) {
    let cars_made_before_2000 = []
    for (let carYear of All_cars_year) {
        if (carYear < 2000) {
            cars_made_before_2000.push(carYear)
        }
    }
    return cars_made_before_2000
}
module.exports = old_cars
