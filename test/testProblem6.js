const inventory = require('../inventory.js')
const Audi_BMW = require('../problem6.js')
const Actual_output = Audi_BMW(inventory)
const Expected_output = ['Audi', 'Audi', 'BMW', 'BMW', 'Audi', 'Audi']


if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log('list contain only Audi and BMW cars', Actual_output)
} else {
    console.log('Wrong answer')
}