const inventory = require('../inventory.js')
const sorted_cars = require('../problem3.js')

let Actual_output = sorted_cars
let Expected_output = [
  '300m',           '4000cs quattro',       '525',
  '6 series',       'accord',               'aerio',
  'bravada',        'camry',                'cavalier',
  'ciera',          'defender ice edition', 'e-class',
  'econoline e250', 'escalade',             'escort',
  'esprit',         'evora',                'express 1500',
  'familia',        'fortwo',               'g35',
  'galant',         'gto',                  'intrepid',
  'jetta',          'lss',                  'magnum',
  'miata mx-5',     'montero sport',        'mr2',
  'mustang',        'navigator',            'prizm',
  'q',              'q7',                   'r-class',
  'ram van 1500',   'ram van 3500',         'riolet',
  'sebring',        'skylark',              'talon',
  'topaz',          'town car',             'tt',
  'windstar',       'wrangler',             'wrangler',
  'xc70',           'yukon']


if(JSON.stringify(Actual_output) == JSON.stringify(Expected_output)){
    console.log('Car models sorted alphabetically ',Actual_output)
}else{
    console.log('Answer is wrong')
}
