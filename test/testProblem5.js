const inventory = require('../inventory.js')
const All_car_years = require('../problem4.js')
const cars_made_before_2000 = require('../problem5.js')

let All_cars_year = All_car_years(inventory)
const Actual_output = cars_made_before_2000(All_cars_year)

const Expected_output = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
  ]

  if(JSON.stringify(Actual_output) == JSON.stringify(Expected_output)){
      console.log('Cars made before the year of 2000',Actual_output,"Cars count",Actual_output.length)
  }else{
      console.log('Wrong Answer')
  }